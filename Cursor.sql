do $$
declare
	registro record;
	cursor_registro cursor for select 
	nombres_tio as nombre, 
	extract (year from gasto.fecha_gasto) as año,
	sum(gasto)as gasto,
	count (gasto) as Total_Aprobado_por_Año
	from gasto
	inner join tio on tio.id_tio = gasto.id_tio
	group by tio.nombres_tio, extract (year from gasto.fecha_gasto)
	order by extract (year from gasto.fecha_gasto);

begin 

for registro in cursor_registro loop
	raise notice 'Tio: %		año: %	cantidad_dinero: $ %		Aprociones_de_gasto: %',
	registro.nombre, registro.año, registro.gasto, registro.Total_Aprobado_por_Año;
end loop;

end $$
language 'plpgsql';