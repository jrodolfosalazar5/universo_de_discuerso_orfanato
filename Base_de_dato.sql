drop table programa
/*==============================================================*/
/* Table: DOCTOR                                                */
/*==============================================================*/
create table DOCTOR (
   ID_DOCTOR            INT4                 not null,
   NOMBRE_DOCTOR        VARCHAR(50)          null,
   TEFEFONO_DOCTOR      VARCHAR(50)          null,
   constraint PK_DOCTOR primary key (ID_DOCTOR)
);

/*==============================================================*/
/* Table: EVALUACION                                            */
/*==============================================================*/
create table EVALUACION (
   ID_EVALUACION        INT4                 not null,
   ID_DOCTOR            INT4                 not null,
   ID_HUERFANO          INT4                 not null,
   FECHA_EVALUACION     DATE                 null,
   NIVEL_SALUD          VARCHAR(50)          null,
   NIVEL_SAOCIAL        VARCHAR(50)          null,
   NIVEL_COGNITIVO      VARCHAR(50)          null,
   constraint PK_EVALUACION primary key (ID_EVALUACION)
);

/*==============================================================*/
/* Table: FAMILIA_ADOPTIVA                                      */
/*==============================================================*/
create table FAMILIA_ADOPTIVA (
   ID_ADOPTIVA          INT4                 not null,
   NOMBRES_PADRE        VARCHAR(50)          null,
   NOMBRES_MADRE        VARCHAR(50)          null,
   CEDULA_PADRE         VARCHAR(50)          null,
   CEDULA_MADRE         VARCHAR(50)          null,
   DIRECCION_           VARCHAR(50)          null,
   TELEFONO_UNO         VARCHAR(10)          null,
   TELEFONO_DOS         VARCHAR(10)          null,
   ECONOMIA             VARCHAR(50)          null,
   constraint PK_FAMILIA_ADOPTIVA primary key (ID_ADOPTIVA)
);

/*==============================================================*/
/* Table: FICHA_INGRESO                                         */
/*==============================================================*/
create table FICHA_INGRESO (
   ID_FICHA             INT4                 not null,
   ID_HUERFANO          INT4                 null,
   NOMBRE_INGRESADOR    VARCHAR(50)          null,
   COMPORTAMIENTO       VARCHAR(50)          null,
   LUGAR_ANTERIOR       VARCHAR(50)          null,
   DESCRIPCION_LUGARANTERIOR VARCHAR(50)          null,
   FECHA_INGRESO        DATE                 null,
   año_ingreso          INT4                 null,
   constraint PK_FICHA_INGRESO primary key (ID_FICHA)
);

/*==============================================================*/
/* Table: GASTO                                                 */
/*==============================================================*/
create table GASTO (
   ID_GASTO             INT4                 not null,
   ID_HUERFANO          INT4                 not null,
   GASTO                MONEY                null,
   DETALLE_GASTO        VARCHAR(50)          null,
   FECHA_GASTO          DATE                 null,
   año_gasto            INT4                 null,
   constraint PK_GASTO primary key (ID_GASTO)
);

/*==============================================================*/
/* Table: HUERFANO                                              */
/*==============================================================*/
create table HUERFANO (
   ID_HUERFANO          INT4                 not null,
   ID_DOCTOR            INT4                 not null,
   NOMBRE_HUERFANO      VARCHAR(50)          null,
   APELLIDO_HUERFANO    VARCHAR(50)          null,
   CEDULA_HUERFANO      VARCHAR(10)          null,
   GENERO               VARCHAR(50)          null,
   ETNIA                VARCHAR(50)          null,
   FECHA_NACIMIENTO     DATE                 null,
   constraint PK_HUERFANO primary key (ID_HUERFANO)
);

/*==============================================================*/
/* Table: HUERFANO_FAMILIAADOPTIVA                              */
/*==============================================================*/
create table HUERFANO_FAMILIAADOPTIVA (
   ID_ADOPTIVA          INT4                 not null,
   ID_HUERFANO          INT4                 not null,
   constraint PK_HUERFANO_FAMILIAADOPTIVA primary key (ID_ADOPTIVA, ID_HUERFANO)
);

/*==============================================================*/
/* Table: HUERFANO_TIO                                          */
/*==============================================================*/
create table HUERFANO_TIO (
   ID_HUERFANO          INT4                 not null,
   ID_TIO               INT4                 not null,
   constraint PK_HUERFANO_TIO primary key (ID_HUERFANO, ID_TIO)
);

/*==============================================================*/
/* Table: PATOLOGIA                                             */
/*==============================================================*/
create table PATOLOGIA (
   ID_PATALOGIAS        INT4                 not null,
   ID_TRATAMIENTO       INT4                 not null,
   ID_FICHA             INT4                 not null,
   DESCRIPCION_PATOLOGIA VARCHAR(50)          null,
   constraint PK_PATOLOGIA primary key (ID_PATALOGIAS)
);

/*==============================================================*/
/* Table: PROGRAMA                                              */
/*==============================================================*/
create table PROGRAMA (
   ID_PROGRAMA          INT4                 not null,
   ID_HUERFANO          INT4                 not null,
   DETALLE_PROGRAMA     VARCHAR(50)          null,
   RENDIMIENTO_PROGRAMA VARCHAR(50)          null,
   FECHA_PROGRAMA       DATE                 null,
   año_programa          INT4                 null,
   constraint PK_PROGRAMA primary key (ID_PROGRAMA)
);

/*==============================================================*/
/* Table: SEGUIMIENTO                                           */
/*==============================================================*/
create table SEGUIMIENTO (
   ID_SEGUIMIENTO       INT4                 not null,
   ID_TIO               INT4                 not null,
   ID_ADOPTIVA          INT4                 not null,
   DETALLE_SEGUIEMIENTO VARCHAR(50)          null,
   CALIFICACION_        VARCHAR(50)          null,
   FECHA_SEGUIMIENTO    DATE                 null,
   año_seguimiento        INT4                null,
   constraint PK_SEGUIMIENTO primary key (ID_SEGUIMIENTO)
);

/*==============================================================*/
/* Table: TIO                                                   */
/*==============================================================*/
create table TIO (
   ID_TIO               INT4                 not null,
   ID_TIPOTIO           INT4                 not null,
   NOMBRES_TIO          VARCHAR(50)          null,
   CEDULA_TIO           VARCHAR(10)          null,
   TELEFONO_TIO         VARCHAR(10)          null,
   DIRECCION            VARCHAR(50)          null,
   constraint PK_TIO primary key (ID_TIO)
);

/*==============================================================*/
/* Table: TIPO_TIO                                              */
/*==============================================================*/
create table TIPO_TIO (
   ID_TIPOTIO           INT4                 not null,
   DETALLE_TIPOTIO      VARCHAR(50)          null,
   constraint PK_TIPO_TIO primary key (ID_TIPOTIO)
);

/*==============================================================*/
/* Table: TRATAMIENTO                                           */
/*==============================================================*/
create table TRATAMIENTO (
   ID_TRATAMIENTO       INT4                 not null,
   DETALLE_TRATAMIENTO  VARCHAR(50)          null,
   constraint PK_TRATAMIENTO primary key (ID_TRATAMIENTO)
);


alter table EVALUACION
   add constraint FK_EVALUACI_RELATIONS_DOCTOR foreign key (ID_DOCTOR)
      references DOCTOR (ID_DOCTOR)
      on delete restrict on update restrict;

alter table EVALUACION
   add constraint FK_EVALUACI_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;

alter table FICHA_INGRESO
   add constraint FK_FICHA_IN_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;

alter table GASTO
   add constraint FK_GASTO_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;


alter table HUERFANO
   add constraint FK_HUERFANO_RELATIONS_DOCTOR foreign key (ID_DOCTOR)
      references DOCTOR (ID_DOCTOR)
      on delete restrict on update restrict;

alter table HUERFANO_FAMILIAADOPTIVA
   add constraint FK_HUERFANO_RELATIONS_FAMILIA_ foreign key (ID_ADOPTIVA)
      references FAMILIA_ADOPTIVA (ID_ADOPTIVA)
      on delete restrict on update restrict;

alter table HUERFANO_FAMILIAADOPTIVA
   add constraint FK_HUERFANO_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;

alter table HUERFANO_TIO
   add constraint FK_HUERFANO_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;

alter table HUERFANO_TIO
   add constraint FK_HUERFANO_RELATIONS_TIO foreign key (ID_TIO)
      references TIO (ID_TIO)
      on delete restrict on update restrict;

alter table PATOLOGIA
   add constraint FK_PATOLOGI_RELATIONS_TRATAMIE foreign key (ID_TRATAMIENTO)
      references TRATAMIENTO (ID_TRATAMIENTO)
      on delete restrict on update restrict;

alter table PATOLOGIA
   add constraint FK_PATOLOGI_RELATIONS_FICHA_IN foreign key (ID_FICHA)
      references FICHA_INGRESO (ID_FICHA)
      on delete restrict on update restrict;

alter table PROGRAMA
   add constraint FK_PROGRAMA_RELATIONS_HUERFANO foreign key (ID_HUERFANO)
      references HUERFANO (ID_HUERFANO)
      on delete restrict on update restrict;

alter table SEGUIMIENTO
   add constraint FK_SEGUIMIE_RELATIONS_TIO foreign key (ID_TIO)
      references TIO (ID_TIO)
      on delete restrict on update restrict;

alter table SEGUIMIENTO
   add constraint FK_SEGUIMIE_RELATIONS_FAMILIA_ foreign key (ID_ADOPTIVA)
      references FAMILIA_ADOPTIVA (ID_ADOPTIVA)
      on delete restrict on update restrict;

alter table TIO
   add constraint FK_TIO_RELATIONS_TIPO_TIO foreign key (ID_TIPOTIO)
      references TIPO_TIO (ID_TIPOTIO)
      on delete restrict on update restrict;




/*==============================================================*/
/* INGRESO DATOS DOCTOR                                         */
/*==============================================================*/
INSERT INTO DOCTOR VALUES(1,'Pedro Sanzhez','0984563115');
INSERT INTO DOCTOR VALUES(2,'Francisco Oliveira','0984632415');
INSERT INTO DOCTOR VALUES(3,'Elena Fuente','0986325681');

/*==============================================================*/
/* Table: TRATAMIENTO                                           */
/*==============================================================*/
INSERT INTO TRATAMIENTO VALUES(1,'recetar inhaladores');
INSERT INTO TRATAMIENTO VALUES(2,'recetar inyecciones');
INSERT INTO TRATAMIENTO VALUES(3,'recetar insulina');
INSERT INTO TRATAMIENTO VALUES(4,'recetar 3 comidas diarias');

/*==============================================================*/
/* Table: TIPO_TIO                                              */
/*==============================================================*/
INSERT INTO TIPO_TIO VALUES(1,'Normal');
INSERT INTO TIPO_TIO VALUES(2,'Administrador');

/*==============================================================*/
/* INGRESO DATOS HUERFANO                               		*/
/*==============================================================*/
INSERT INTO HUERFANO VALUES(1,1,'Manolito','Cedeño','1316541239','niño','mestizo','2015/02/12');
INSERT INTO HUERFANO VALUES(2,2,'Federico','loor','1315264892','niño','mestizo','2014/06/15');
INSERT INTO HUERFANO VALUES(3,2,'Maria','Aria','1312031684','niña','mestizo','2013/09/20');
INSERT INTO HUERFANO VALUES(4,3,'Vicente','Vera','1310213659','niño','mestizo','2015/03/17');
INSERT INTO HUERFANO VALUES(5,3,'Patricia','Lopez','1312564891','niña','mestizo','2014/05/28');

/*==============================================================*/
/* INGRESO DATOS FICHA_INGRESO                              */
/*==============================================================*/
INSERT INTO FICHA_INGRESO VALUES(1,1,'Raul Morales','bueno','Casa hogar','Muy aglomerado','2020/02/13',2020);
INSERT INTO FICHA_INGRESO VALUES(2,2,'Ramon Valdes','bueno','Casa hogar','Muy aglomerado','2020/06/12',2020);
INSERT INTO FICHA_INGRESO VALUES(3,3,'Enrique Araus','malo','Vecinos','Casa en mal estado','2020/07/21',2020);
INSERT INTO FICHA_INGRESO VALUES(4,4,'Javier Cedeño','regular','Vecinos','Casa en mal estado','2021/01/10',2021);
INSERT INTO FICHA_INGRESO VALUES(5,5,'Carlos Salazar','bueno','Casa hogar','Muy aglomerado','2021/02/28',2021);

/*==============================================================*/
/* INGRESO DATOS EVALUACION                                      */
/*==============================================================*/
INSERT INTO EVALUACION VALUES(1,1,1,'2020/05/10','buena','buena','buena');
INSERT INTO EVALUACION VALUES(2,2,2,'2020/11/14','buena','regular','buena');
INSERT INTO EVALUACION VALUES(3,2,3,'2020/11/20','buena','regular','mala');
INSERT INTO EVALUACION VALUES(4,3,4,'2021/03/10','buena','mala','regular');
INSERT INTO EVALUACION VALUES(5,3,5,'2021/04/20','buena','buena','regular');

/*==============================================================*/
/* INGRESO DATOS GASTO                              */
/*==============================================================*/
INSERT INTO GASTO VALUES(1,1,30,'cumpleaños','2020/02/22',2020);
INSERT INTO GASTO VALUES(2,2,40,'fiesta','2020/08/21',2020);
INSERT INTO GASTO VALUES(3,3,50,'cumpleaños','2020/03/25',2020);
INSERT INTO GASTO VALUES(4,4,35,'cumpleaños','2021/09/30',2021);
INSERT INTO GASTO VALUES(5,5,25,'fista','2021/07/12',2020);
INSERT INTO GASTO VALUES(6,1,25,'cumpleaños','2020/03/13',2020);
INSERT INTO GASTO VALUES(7,2,40,'fiesta','2020/04/13',2020);
INSERT INTO GASTO VALUES(8,3,30,'cumpleaños','2021/05/13',2021);
INSERT INTO GASTO VALUES(9,4,55,'cumpleaños','2021/06/13',2021);
INSERT INTO GASTO VALUES(10,5,60,'fista','2021/07/13',2021);

/*==============================================================*/
/* Table: PROGRAMA                                              */
/*==============================================================*/
INSERT INTO PROGRAMA VALUES(1,1,'social','bueno','2020/05/27',2020);
INSERT INTO PROGRAMA VALUES(2,2,'deportivo','regular','2020/07/13',2020);
INSERT INTO PROGRAMA VALUES(3,2,'deportivo','regular','2020/08/13',2020);
INSERT INTO PROGRAMA VALUES(4,3,'academico','bueno','2020/10/11',2020);
INSERT INTO PROGRAMA VALUES(5,2,'social','regular','2020/10/20',2020);
INSERT INTO PROGRAMA VALUES(6,1,'academico','irregular','2021/03/14',2021);
INSERT INTO PROGRAMA VALUES(7,1,'academico','irregular','2021/05/14',2021);
INSERT INTO PROGRAMA VALUES(8,4,'social','regular','2021/11/13',2021);
INSERT INTO PROGRAMA VALUES(9,5,'deportivo','bueno','2021/05/13',2021);
INSERT INTO PROGRAMA VALUES(10,5,'deportivo','bueno','2021/05/13',2021);

/*==============================================================*/
/* Table: TIO                                                   */
/*==============================================================*/
INSERT INTO TIO VALUES(1,1,'Eduardo Meza','1311326489','095123548','Barrio Jokay');
INSERT INTO TIO VALUES(2,2,'Mateo Ortiz','1302156987','091256301','Eloy Alfaro');
INSERT INTO TIO VALUES(3,2,'Rodolfo Ponce','1302163021','098457136','4 de Nobriembre');

/*==============================================================*/
/* Table: HUERFANO_TIO                                          */
/*==============================================================*/
INSERT INTO HUERFANO_TIO VALUES(1,1);
INSERT INTO HUERFANO_TIO VALUES(2,1);
INSERT INTO HUERFANO_TIO VALUES(3,2);
INSERT INTO HUERFANO_TIO VALUES(4,1);
INSERT INTO HUERFANO_TIO VALUES(5,3);


/*==============================================================*/
/* INGRESO DATOS FAMILIA_ADOPTIVA                               */
/*==============================================================*/
INSERT INTO FAMILIA_ADOPTIVA VALUES(1,'Roberto Velez','Martha Hernan','1315469812','1315648236','Urbirrios','0946312549','0945823014','buena');
INSERT INTO FAMILIA_ADOPTIVA VALUES(2,'Carlos Santana','Martha Hernan','1315469812','1315648236','Los esteros','0946312549','0945823014','buena');


/*==============================================================*/
/* Table: HUERFANO_FAMILIAADOPTIVA                              */
/*==============================================================*/
INSERT INTO HUERFANO_FAMILIAADOPTIVA VALUES(1,3);
INSERT INTO HUERFANO_FAMILIAADOPTIVA VALUES(2,5);


/*==============================================================*/
/* Table: PATOLOGIA                                             */
/*==============================================================*/
INSERT INTO PATOLOGIA VALUES(1,1,1,'asmatico');
INSERT INTO PATOLOGIA VALUES(2,2,2,'reumatismo');
INSERT INTO PATOLOGIA VALUES(3,3,3,'diabetes infantil');
INSERT INTO PATOLOGIA VALUES(4,4,4,'desnutricion');
INSERT INTO PATOLOGIA VALUES(5,2,5,'reumatismo');


/*==============================================================*/
/* Table: SEGUIMIENTO                                           */
/*==============================================================*/
INSERT INTO SEGUIMIENTO VALUES(1,2,1,'se inspecciono la casa','bueno','2020/03/29',2020);
INSERT INTO SEGUIMIENTO VALUES(2,2,1,'se inspecciono la casa','regular','2020/03/29',2020);
INSERT INTO SEGUIMIENTO VALUES(3,3,2,'se inspecciono la casa','regular','2020/04/29',2020);
INSERT INTO SEGUIMIENTO VALUES(4,3,2,'se inspecciono la casa','bueno','2021/05/29',2021);
INSERT INTO SEGUIMIENTO VALUES(5,2,1,'se inspecciono la casa','bueno','2021/06/29',2021);
INSERT INTO SEGUIMIENTO VALUES(6,3,2,'se inspecciono la casa','regular','2021/07/29',2021);
INSERT INTO SEGUIMIENTO VALUES(7,2,1,'se inspecciono la casa','bueno','2021/08/29',2021);




/*select 
tio.nombres_tio as Nombres,
tipo_tio.detalle_tipotio as Cargo,
seguimiento.año_seguimiento as año,
count (seguimiento) as cantidad_de_seguimiento
from seguimiento
inner join tio on tio.id_tio = seguimiento.id_tio
inner join tipo_tio on tipo_tio.id_tipotio = tio.id_tipotio
group by tio.nombres_tio, seguimiento.año_seguimiento, tipo_tio.detalle_tipotio
order by año_seguimiento*/

/*select
ficha_ingreso.año_ingreso as Año,
count (huerfano) as Total_ingresados
from ficha_ingreso
inner join huerfano on huerfano.id_huerfano = ficha_ingreso.id_huerfano
group by ficha_ingreso.año_ingreso*/


/*select 
huerfano.genero,
gasto.año_gasto as Año,
sum (gasto.gasto)
from gasto
inner join huerfano on huerfano.id_huerfano = gasto.id_huerfano
group by huerfano.genero, gasto.año_gasto*/


/*select
programa.detalle_programa as Programa,
programa.año_programa as Año,
programa.rendimiento_programa as Rendimiento,
count(programa) as cantidad
from programa
group by programa.detalle_programa, programa.año_programa, programa.rendimiento_programa
order by año_programa*/


