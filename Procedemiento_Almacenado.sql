create or replace function candidato_adopcion(in nombre varchar) returns
table (
		padre varchar,
		madre varchar,
		fecha date,
		estado varchar)
as $$
begin
	return query 
	select
	FAMILIA_ADOPTIVA.NOMBRES_PADRE,
	FAMILIA_ADOPTIVA.NOMBRES_MADRE,
	PROCESO_ADOPCION.FECHA_PROCESO,
	PROCESO_ADOPCION.ESTADO_PROCESO
	FROM PROCESO_ADOPCION
	inner join FAMILIA_ADOPTIVA ON FAMILIA_ADOPTIVA.ID_ADOPTIVA = PROCESO_ADOPCION.ID_ADOPTIVA
	inner join HUERFANO on HUERFANO.ID_HUERFANO = PROCESO_ADOPCION.ID_HUERFANO
	where huerfano.nombre_huerfano= "nombre";
end $$
language 'plpgsql';
select * from candidato_adopcion('Maria');
select * from candidato_adopcion('Vicente');
