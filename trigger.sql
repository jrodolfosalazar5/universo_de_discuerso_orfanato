create or replace function gastovalor()returns trigger as $gastovalor$
declare
valorgasto int;
valormensualidad int;
begin

	select gasto into valorgasto from gasto where id_gasto = new.id_gasto;
	select mesada into valormensualidad from gasto;
	if (valormensualidad - valorgasto < 0) then
	raise exception 'No se puede realizar el gasto para realizar el compromiso debido que la mesada no alcanza';
	end if;
	return new;

end;
$gastovalor$
language plpgsql;
create trigger trigger_gasto before insert on gasto for each row
execute procedure gastovalor();

insert into gasto values (11,5,60,'cumpleaños','2021/11/25',50)
