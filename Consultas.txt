---------Consulta 1-------
/*select 
tio.nombres_tio as Nombres,
tipo_tio.detalle_tipotio as Cargo,
seguimiento.año_seguimiento as año,
count (seguimiento) as cantidad_de_seguimiento
from seguimiento
inner join tio on tio.id_tio = seguimiento.id_tio
inner join tipo_tio on tipo_tio.id_tipotio = tio.id_tipotio
group by tio.nombres_tio, seguimiento.año_seguimiento, tipo_tio.detalle_tipotio
order by año_seguimiento*/

-----Consulta 2------
/*select
ficha_ingreso.año_ingreso as Año,
count (huerfano) as Total_ingresados
from ficha_ingreso
inner join huerfano on huerfano.id_huerfano = ficha_ingreso.id_huerfano
group by ficha_ingreso.año_ingreso*/


------consulta 3-------
/*select 
huerfano.genero,
gasto.año_gasto as Año,
sum (gasto.gasto)
from gasto
inner join huerfano on huerfano.id_huerfano = gasto.id_huerfano
group by huerfano.genero, gasto.año_gasto*/


-------Consulta 4------
/*select
programa.detalle_programa as Programa,
programa.año_programa as Año,
programa.rendimiento_programa as Rendimiento,
count(programa) as cantidad
from programa
group by programa.detalle_programa, programa.año_programa, programa.rendimiento_programa
order by año_programa*/